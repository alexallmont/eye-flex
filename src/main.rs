// Experimental app for training eye muscles. Requires red and blue 3D glasses.
// A simple image of noise is drawn in red and blue and moved with the mouse x position.

use bevy::{
    input::mouse::MouseMotion,
    prelude::*,
    sprite::MaterialMesh2dBundle,
    window::{PresentMode, close_on_esc}
};

// Components for tracking the two squares
#[derive(Component)]
struct RedSquare;

#[derive(Component)]
struct BlueSquare;

// Main app is a fullscreen app with a black background. Escape exits.
fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(0.0, 0.0, 0.0)))
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            window: WindowDescriptor {
                title: "Eye-Flex".to_string(),
                present_mode: PresentMode::AutoVsync,
                mode: WindowMode::BorderlessFullscreen,
                ..default()
            },
            ..default()
        }))
        .add_startup_system(setup)
        .add_system(close_on_esc)
        .add_system(mouse_events_red)
        .add_system(mouse_events_blue)
        .run();
}

// Create the 2D camera and coloured images to render
fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    asset_server: Res<AssetServer>
) {
    commands.spawn(Camera2dBundle::default());

    let sprite_handle = asset_server.load("noise.png");

    commands.spawn((
        MaterialMesh2dBundle {
            mesh: meshes.add(Mesh::from(shape::Quad::default())).into(),
            transform: Transform::from_xyz(0.0, 0.0, 0.0).with_scale(Vec3::splat(512.0)),
            material: materials.add(
                ColorMaterial {
                    color: Color::rgba(0.5, 0.0, 0.0, 0.5),
                    texture: Some(sprite_handle.clone()),
                }
            ),
            ..Default::default()
        },
        RedSquare
    ));

    commands.spawn((
        MaterialMesh2dBundle {
            mesh: meshes.add(Mesh::from(shape::Quad::default())).into(),
            transform: Transform::from_xyz(0.0, 0.0, 0.0).with_scale(Vec3::splat(512.0)),
            material: materials.add(
                ColorMaterial {
                    color: Color::rgba(0.0, 0.0, 1.0, 0.5),
                    texture: Some(sprite_handle.clone()),
                }
            ),
            ..Default::default()
        },
        BlueSquare,
    ));
}

// Update the red square with mouse deltas
fn mouse_events_red(
    mut mouse_motion_events: EventReader<MouseMotion>,
    mut red_tx_query: Query<&mut Transform, With<RedSquare>>
) {
    for event in mouse_motion_events.iter() {
        let mut red_tx = red_tx_query.single_mut();
        red_tx.translation.x += event.delta.x / 10.0;
    }
}

// Update the blue square with mouse deltas
fn mouse_events_blue(
    mut mouse_motion_events: EventReader<MouseMotion>,
    mut blue_tx_query: Query<&mut Transform, With<BlueSquare>>,
) {
    for event in mouse_motion_events.iter() {
        let mut blue_tx = blue_tx_query.single_mut();
        blue_tx.translation.x -= event.delta.x / 10.0;
    }
}
