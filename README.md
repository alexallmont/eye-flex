# Eye-Flex

## Experimental eye muscle developer

To be used with 3D glasses, assuming blue in left lens and red in right. This is an experiment to see if I can encourage eye muscle development to reduce my growing need for prisms in my glasses, after my many years of close monitor use.

The app renders a blue and red square of the same noisy image, and moves them away from each other given the mouse movement x delta.
Your eyes will individually track the two images, seeing them as one, until you reach the point where they cannot move further, at which point you will see the image 'stretch' horizontally rather than staying square, as the two images move beyond your physical ability.

**Use at your own risk, this a personal experiment and I am not an optician!**

## Running

```
cargo run --release
```

You will see a black fullscreen with overlaid images. Move the mouse to shift their distance.

![alt text](screenshot.png "Screenshot")

Moving the mouse left has the effect of moving the eyes closer together; going 'cross-eyed' as the red right eye looks more to the left and the blue left eye looks to the right. Moving the mouse right moves them apart, and this what I am most keen to train, to see if it encourages muscle development for distance viewing.

Press escape to exit.

## TODO

- Replace sprite-like transparency overlay with an additive shader.
